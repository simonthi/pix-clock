# Pix Clock

A clock that shows the time by showing photos with the time on it. Sure, this can be done by a robot, but it’s much more fun to do it by hand.

Here’s [a working example of this Pix Clock](https://vasilis.nl/clocks/pix-clock/)

If there’s an image with a clock of this exact timestamp it shows it. If this image does not exist, it shows *the next image*. It’s either right on time, or it’s (a bit) fast. If there are more images for one timestamp it will pick one randomly.


## Contributing

You can add your own pictures if you want to (and [if you agree with our choses licences](https://codeberg.org/vasilis/pix-clock#licences)). Guidelines (not rules) for pictures:

- No (recognisable) people on it
- A clock is seen
- The time can be read
- It’s interesting to look at
- Unique pictures are preferred (i.e. don’t take the same picture every minute)

Pictures *must* be your own and you *must* agree to publish them under the [Creative Commons CC BY-NC 4.0 licence](https://creativecommons.org/licenses/by-nc/4.0/).

You can add pictures by making a pull request. Make sure you understand how the files should be named. If you don’t know how to make a pull request you can also send the images via email to `pixclock at vasilis dot nl`. *Make sure to use a service like Wetransfer*.

### Naming files

It’s rather simple. Files are named as follows:

```
hh_mm_whatever.jpg
```

So for instance, a clock that points at half past seven can be named like `07_30_made-by-me.jpg`. 
In most cases this should be fine. *Make sure that you use 07, and not 19.* Except when it’s a unique clock.

A clock can be unique for a single time stamp, or a clock can be shown for both AM and PM. See the [Is it AM or MP section for a better explanation](https://codeberg.org/vasilis/pix-clock#is-it-am-or-pm).

When a clock is unique, for instance when a digital clock reads *19:30*, the naming scheme is a little different:

```
hh_mm_whatever_1x.jpg
```

Note the _1x before the JPG extension. This tells the script to only use this image for the unique timestamp. So in this example the file would be called something like `19_30_i-woke-up-at_1x.jpg`.

### Please optimise your images

If possible, please use a tool like JPEGmini to optimise the file size of your images. 

#### Srcset

If an image is rather large you should consider making multiple versions of the file. There are three so called `srcset` directories: 1600, 2400 and 3000. These numbers look at the longest side of the image. So please resize your images to versions with a longest side of 1600, 2400 and 3000. 

### Please add an alt text

When an image cannot be displayed, an alternative text is read. The major use case for this feature is for people who use a screen reader, mostly people who are blind. I believe the web should be optimised for everybody, so it should work as nice for people who depend on screen readers as it does for you.

In a good alternative text you describe the image as if you’re explaining what you see over the phone, but just assume you’re in a bit of a hurry. So no endless observations about details, unless those details are what matter. What matters here is *at least* the time.

If you don’t add an alt text, the default alt text will be: *It is 13:37* (if it is 13:37 on that image, of course).

You can add an alt text manually in the `clock-pix/` directory. Just give them the same name as the image, but with an extension of .txt. So `00_00_00.jpg` becomes `00_00_00.jpg.txt`.

### Is it AM or PM?

Sometimes it is unclear if a picture is taken before or after noon. In that case it’s possible to duplicate it. Rules of thumb for duplication:

- We use the so called Civil Twilight zone of the Netherlands, which means that from ±4:30 AM till ±22:58 a clear sky may be visible, and from 16:27 till 08:09 a dark sky is possible.
- If no sky is visible it could also be another light source.

## Contributions

This is based on an idea by Erik van Blokland. Code is written by Vasilis van Gemert

Pictures contributed by

- Erik van Blokland
- Vasilis van Gemert
- Florian Hardwig
- Kiki
- David Krooshof

## Installing

Download everything, upload it to a server that runs PHP and visit the URL. It’s that easy.

## Licences

- The code in this repository is licenced under the [Unlicence](https://unlicense.org/)
- The content (the images, photos, text) in this repository is licenced under the [Creative Commons CC BY-NC 4.0 licence](https://creativecommons.org/licenses/by-nc/4.0/).
