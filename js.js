// Every minute some classes are updated
// Only the images in the current hour, no need for more. For now.
// Things will probably get more complicated soon, as always with time

function updateImg(h,m,r) {
	h = h % 24;
	
	// we’re looking for the previous timestamp photo or the current one
	// so let’s also check the previous hour.
	// And yes, the previous hour can be 23
	var previousH = (h*1)-1;
	if (previousH == -1) {
		previousH = 23;
	}
	// add a leading 0 if smaller than 10
	if(previousH < 10) {
		previousH = "0" + previousH;
	}
	// loop through all the images
	var divs = document.querySelectorAll('div');
	var i = 0;
	while(i < divs.length) {
		// Split things to get hours and minutes
		divSplit = divs[i].getAttribute('data-time').split(':');
		
		divH = (divSplit[0] * 1);
		divM = (divSplit[1] * 1);
		
		// This should only happen onload, not every minute
		// Here we decide which image will be shown, 
		// if there are more than one image for a certain time stamp
		if(r == 0 && divs[i].children.length > 1) {
			divs[i].setAttribute('data-show',getRandomInt(1,divs[i].children.length));
		}
		
		
		// Now, if the image is in the previous hour …
		// it could still be the last timestamp
		if (previousH == divH) {
			divs[i].classList.add('was');
		}
		// But once it’s 01:00 remove .was from 23:00.
		if (divH > h && previousH !== divH) {
			divs[i].classList.remove('was');
		}
		// if it’s in the current hour …
		if (h == divH) {
			// and it’s timestamp is smaller that the current minute …
			if ( m > divM ) {
				divs[i].classList.add('was');
				divs[i].classList.remove('is');
				divs[i].classList.remove('willbe');
			}
			// Don’t show too many images
			while (document.querySelectorAll('.was').length > 10) {
				document.querySelector('.was').classList.remove('was');
			}
			// if it’s exactly the current minute
			if (m == divM) {
				if (document.querySelector('.is')) {
					document.querySelector('.is').classList.remove('is');
				}
				divs[i].classList.remove('willbe');
				divs[i].classList.add('is');
			}
		}
		if (divH == 0) {
			divs[i].classList.add('today');
		}
		
		i++;
	}
	
	// Now let’s try to show the next image if there’s not image for the current time …
	
	hh = h;
	mm = m;
	if (h < 10) { hh = "0" + h; }
	if (m < 10) { mm = "0" + m; }
	
	if (!document.querySelector('[data-time="'+hh+':'+mm+'"]')) {
		
		n = 0;
		nm = m;
		// 14 seems arbitrary. It’s the longest sequence of minutes where images are missing
		while (n < 14) {
			nm = m + n;
			nh = h;
			// do some hour and day switching logic
			if (nm > 59) {
				nm = nm % 60;
				nh = h + 1;
			}
			if (nh > 23) {
				nh = 0;
			}
			
			// Add zeroes if smaller that 10.
			hh = nh;
			mm = nm;
			if (nh < 10) { hh = "0" + hh; }
			if (nm < 10) { mm = "0" + mm; }
			
			
			if (document.querySelector('[data-time="'+hh+':'+mm+'"]')) {
				
				if (document.querySelector('.is')) {
					document.querySelector('.is').classList.remove('is');
				}
				document.querySelector('[data-time="'+hh+':'+mm+'"]').classList.add('willbe');
				break;
			}
			n++;
		}
		
	}
	
	
	
	// Make sure it works on screen readers as well.
	
	// First get the image that’s shown right now.
	// It has a class of either .was or .is
	// And it’s the last one of those.
	var wasis = document.querySelectorAll('.was,.is');
	var wl = wasis.length;
	var currentWasIs = wasis[wl-1];
	
	// When there are more than one img for a timestamp
	// Find out which one is used. It’s a number.
	var nth = 1;
	if(currentWasIs.getAttribute('data-show')) {
		nth = currentWasIs.getAttribute('data-show');
	}
	// Get the alt text from the image
	// And put it in the aria live region.
	var imgAlt = currentWasIs.querySelector('img:nth-of-type('+nth+')').getAttribute('alt');
	var liveRegion = document.querySelector('[aria-live="polite"]');
	var liveContent = liveRegion.innerText;
	if(imgAlt !== liveContent) {
		liveRegion.innerText = imgAlt;
	}
}



// Onload, trigger the script
//var d = new Date(Date.now() + (6 * 60 * 1000)); // Fast forward
var d = new Date();
var h = curH = d.getHours();
var m = curM = d.getMinutes();


// Zero, to trigger randomness
updateImg(h,m,0);

// Every second this script checks if the minute has changed.
setInterval( function(){
	nowM = new Date().getMinutes();
	// if the minute is up, trigger the script with new tome stamps.
	if (nowM !== curM) {
		var d = new Date();
		var h = curH = d.getHours();
		var m = curM = d.getMinutes();
		
		// 1, because we don’t want random every minute
		updateImg(h,m,1);
	}
 },1000);
 
 
 // Simple min-max-random function.
 
 function getRandomInt(min, max) {
	 min = Math.ceil(min);
	 max = Math.floor(max);
	 return Math.floor(Math.random() * (max - min + 1)) + min;
 }