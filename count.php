<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Show the timestamps and if they’re missing</title>
	<style>
		body {
			display: grid;
			grid-template-columns: repeat(auto-fill,minmax(20em,1fr));
			gap: 1em;
			counter-reset: hoi;
		}
		h2 {
			margin: 0;
			
		}
		ul {
			display: grid;
			grid-template-columns: repeat(10, 2em);
			list-style: none;
			text-align: right;
			margin: 0;
			padding: 0;
			counter-reset: ul;
			
		}
		ul:after {
			content: counter(ul);
		}
		[data-count] {
			background: limegreen;
			padding: .2em;
		}
		[data-count="0"] {
			background: crimson;
			color: white;
			counter-increment: hoi ul;
		}
		body:after {
			content: counter(hoi);
		}
	</style>
</head>


<body>

<?php

$singleFiles = glob('clock-pix/*_1x.jpg');
$doubleFiles = glob('clock-pix/*.jpg');
$count = ((count($doubleFiles) - count($singleFiles)) * 2 + count($singleFiles)) ;

$i = 0;
$j = 0;
$files = [];
while ($i < count($doubleFiles)) {
	$single = explode('1x', $doubleFiles[$i]);
	if ( count($single) == 1 ) {
		$tt = explode('clock-pix/', $doubleFiles[$i]);
		$th = explode('_', $tt[1]);
		$thh = ($th[0] * 1) + 12;
		
		$files[$j] = 'clock-pix/'. $thh;
		$k = 1;
		while($k < count($th)) {
			$files[$j] .=  '_'. $th[$k];
			$k++;
		} 
		$j++;
	}
	if($doubleFiles[$i]) {
		$files[$j] = $doubleFiles[$i];
		$j++;
	}
	$i++;
}
rsort($files);
//var_dump($files);

$i = 0;
while ($i < 24){
	echo "<section><h2>$i</h2>\n<ul>";
	$h = $i;
	if ($i < 10) $h = "0" . $i;
	$j = 0;
	while ($j < 60) {
		$m = $j;
		if ($j < 10) $m = "0" . $j;
		$matches  = preg_grep('/\/'.$h.'_'.$m.'_/', $files);
		//var_dump($matches);
		echo "<li data-count='" . count($matches) . "'>$j</li>";
		$j++;
	}
	echo "</ul></section>";
	$i++;
}

//include('generate-cache.php');