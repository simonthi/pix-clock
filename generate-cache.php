<?php


// Loop through all files and put them in an array

$divs = '';

$allFiles = glob('clock-pix/*.jpg'); // All files, including the single files!

$i = 0;
$j = 0;
$files = []; // We need a new array, because the amount of timestamps is not the same as the amount of files

while ($i < count($allFiles)) {
	// There are files that only represent a single timestamp, like 00_01_19_1x.jpg
	$single = explode('1x', $allFiles[$i]);
	// If it’s not a single timestamp, it can be used twice a day
	if ( count($single) == 1 ) {
		// First get the hour
		$tt = explode('clock-pix/', $allFiles[$i]);
		$th = explode('_', $tt[1]);
		// Add twelve to the hour
		$thh = ($th[0] * 1) + 12;
		
		// Add it all together and add it to the array
		$files[$j] = 'clock-pix/'. $thh;
		$k = 1;
		while($k < count($th)) {
			$files[$j] .=  '_'. $th[$k];
			$k++;
		} 
		$j++;
	}
	// Add the file to the file array.
	if($allFiles[$i]) {
		$files[$j] = $allFiles[$i];
		$j++;
	}
	$i++;
}


// Sort the files
sort($files);


$i = 0;
$hm = ''; // a check, should be empty at first
while ($i < count($files)) {
	// Get the time stamps
	$tt = explode('/', $files[$i]);
	$tt = explode('_', $tt[1]);
	
	// This is the time, which will be added to a data-time attribute to the containing div …
	$thm = "$tt[0]:$tt[1]";
	
	$div = '';
	$ffile = $files[$i];
	
	// Here we check if the file exists. If it doesn't, it means that the file is twelve hours ago.
	if (!file_exists("$files[$i]")) {
		// So right here, something like 19_37_01.jpg turns into 07_37_01.jpg
		$hour = $tt[0] * 1 - 12;
		if($hour < 10) {$hour = '0' . $hour;}
		$ffile = str_replace('/'.$tt[0].'_', '/'.$hour.'_', $files[$i]);
	}
	
	// If the timestamp is different that the previous one we need a new div
	if ($thm !== $hm) {
		$div = "</div>\n<div data-time='$thm' aria-hidden='true'>";
		$hm = $thm;
	}
	// Set a default alt text
	$alt = "$tt[0]:$tt[1]";
	if (file_exists("$ffile.txt")) {
		// If there’s a file with a better alt text, use that
		$alt = htmlspecialchars(file_get_contents("$ffile.txt"));
		
	}
	
	// The srcset part
	$src = $ffile; // Set the source of the image to the original file
	$srcset = '';
	$src1600 = str_replace('clock-pix/', 'clock-pix/1600/', $ffile); // 1600 version
	$src2400 = str_replace('clock-pix/', 'clock-pix/2400/', $ffile); // 2400 version
	$src3000 = str_replace('clock-pix/', 'clock-pix/3000/', $ffile); // 3000 version
	list($ffWidth, $ffHeight) = getimagesize($ffile); // width and height of original file
	
	// If there’s a 1600 version of the image …
	if(file_exists($src1600)) {
		$src = $src1600; // Set the source of the image to the 1600 version, which is smaller
		$srcset = ''; // Reset the srcset
		$srcend = ''; // We might need an ending later on …
		list($s16Width, $s16Height) = getimagesize($src1600); // width and height of 1600 file
		
		// If the original file is larger than the 1600 version we need a srcset attribute
		if ($ffWidth > $s16Width) {
			$srcend = "'";
			$srcset = "srcset='$src1600 ". $s16Width ."w, $ffile ". $ffWidth ."w";
		}
		
		// if there’s a 2400 version of the image …
		if(file_exists($src2400)) {
			list($s24Width, $s24Height) = getimagesize($src2400); // width and height of 2400 file
			$sWidth = $s24Width; // We use this later, can change if there’s also a 3000 file
			
			// If the 2400 file is larger than the 1600 version we need to expand the srcset attribute
			if ($s24Width > $s16Width) {
				$srcend = "'";
				$srcset = "srcset='$src1600 ". $s16Width ."w, $src2400 ". $s24Width ."w";
			}
			// if there’s a 2400 version of the image …
			if(file_exists($src3000)) {
				list($s30Width, $s30Height) = getimagesize($src3000); // width and height of 3000 file
				$sWidth = $s30Width; // Overwrite the sWidth value
				
				// If the 2400 file is larger than the 1600 version we need to expand the srcset attribute
				if ($s30Width > $s24Width) {
					$srcset .= ", $src3000 ". $s30Width ."w";
				}
			}
			// If the original file is larger than biggest file so far we need to expand the srcset attribute
			if ($ffWidth > $sWidth) {
				$srcset .= ", $ffile ". $ffWidth ."w";
			}
		}
		
		// Add the srcend to the srcset
		$srcset .= $srcend;
	}
	
	// Write the div to cache.
	$divs .= "$div<img alt='$alt' loading='lazy' $srcset src='$src'>";
	
	$i++;
}

file_put_contents('index.txt', $divs);

?>
ok.