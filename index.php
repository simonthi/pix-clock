<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>The time in Photos</title>
	<!-- ==========================================
		An idea by Erik van Blokland
		Most, if not all, pictures by Erik van Blokland.
		You can add your own pictures: 
		https://codeberg.org/vasilis/pix-clock/
	=========================================== -->
	<link rel="stylesheet" href="css.css?<?php echo filemtime('css.css') ?>">
	
	
	<link rel="apple-touch-icon" sizes="180x180" href="/clocks/pix-clock/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/clocks/pix-clock/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/clocks/pix-clock/favicon-16x16.png">
	<link rel="manifest" href="/clocks/pix-clock/site.webmanifest">
	<link rel="mask-icon" href="/clocks/pix-clock/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="/clocks/pix-clock/favicon.ico">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="PixClock">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
	<meta name="application-name" content="PixClock">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-config" content="/clocks/pix-clock/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	
</head>


<body id="crop">
	<section role="region" aria-live="polite" lang="en"></section>

<div data-time="25:00" aria-hidden="true">
<?php 
//Check how long ago the cache was created
$diff = date('U') - filemtime('index.txt');

// Check if the cache exists. 
// If it does, and if it’s younger that one day …
if (file_exists('index.txt') && $diff < 86400) {
	// echo its contents
	$divs = file_get_contents('index.txt');
	echo $divs;
}
else {
	// generate a new cache and echo its contents
	include('generate-cache.php');
	$divs = file_get_contents('index.txt');
	echo $divs;
}
 ?>
 </div>
 
 
 <script src="js.js?<?php echo filemtime('js.js') ?>"></script>

</body>
</html>